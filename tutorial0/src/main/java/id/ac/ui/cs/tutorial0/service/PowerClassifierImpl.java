package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

@Service
public class PowerClassifierImpl implements PowerClassifierService {

    @Override
    public String classifyPower (int power) {
        if (power >= 0 && power < 20000) {
            return "C class";
        } if (power >= 20000 && power < 100000) {
            return "B class";
        } if (power >= 100000) {
            return "A class";
        } return "undefined";
    }
}
