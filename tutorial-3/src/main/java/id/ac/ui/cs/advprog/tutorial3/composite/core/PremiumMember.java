package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

        private String name;
        private String role;
        private boolean isMaster;
        private final ArrayList<Member> members = new ArrayList<Member>();

        public PremiumMember(String name, String role, boolean isMaster) {
                // Contructor
                this.name = name;
                this.role = role;
                this.isMaster = isMaster;
        }

        public PremiumMember(String name, String role) {
                // Contructor
                this.name = name;
                this.role = role;
                this.isMaster = false;
        }

        public PremiumMember() {
                // Default contructor
                this("", "");
        }

        @Override
        public String getName() {
                return this.name;
        }

        @Override
        public String getRole() {
                return this.role;
        }

        @Override
        public void addChildMember(Member member) {
                if (this.members.size() >= 3) {
                        if (!isMaster) return;
                }

                this.members.add(member);
        }

        @Override
        public void removeChildMember(Member member) {
                this.members.remove(member);
        }

        @Override
        public List<Member> getChildMembers() {
                return this.members;
        }
}
