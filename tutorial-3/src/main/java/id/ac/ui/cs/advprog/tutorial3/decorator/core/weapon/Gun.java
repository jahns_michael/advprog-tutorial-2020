package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
        public Gun() {
                weaponName = "Gun";
                weaponDescription = "Gun adalah senjata mesin mekanik jarak jauh";
                weaponValue = 20;
        }
}
