package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;


public class OrdinaryMember implements Member {


        private String name;
        private String role;
        private final ArrayList<Member> members = new ArrayList<Member>();

        public OrdinaryMember(String name, String role) {
                // Contructor
                this.name = name;
                this.role = role;
        }

        public OrdinaryMember() {
                // Default contructor
                this("", "");
        }

        @Override
        public String getName() {
                // Returns name
                return this.name;
        }

        @Override
        public String getRole() {
                // Returns role
                return this.role;
        }

        @Override
        public void addChildMember(Member member) {
                // Ordinary member : do nothing
                return;
        }

        @Override
        public void removeChildMember(Member member) {
                // Ordinary member : do nothing
                return;
        }

        @Override
        public List<Member> getChildMembers() {
                // Returns list of members
                return this.members;
        }
}
