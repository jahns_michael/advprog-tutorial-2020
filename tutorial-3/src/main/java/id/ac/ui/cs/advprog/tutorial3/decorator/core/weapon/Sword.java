package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        public Sword() {
                weaponName = "Sword";
                weaponDescription = "Sword adalah senjata tajam jarak dekat.";
                weaponValue = 25;
        }
}
