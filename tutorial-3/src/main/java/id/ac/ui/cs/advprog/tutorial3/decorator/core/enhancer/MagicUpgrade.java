package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        Random random = new Random();
        int randInt = random.nextInt(5);
        int addedPoint = 15 + randInt;
        int totalPoint = this.weapon.getWeaponValue() + addedPoint;
        return totalPoint;
    }

    @Override
    public String getDescription() {
        return this.weapon.getDescription() + " (+ magic)";
    }
}
