package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;
    private Weapon shield;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
        shield = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Shield", uniqueUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(shield.getDescription() + " (+ unique)", uniqueUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(10 <= uniqueUpgrade.getWeaponValue()-shield.getWeaponValue() ||
        uniqueUpgrade.getWeaponValue()-shield.getWeaponValue() >= 15);
    }
}
