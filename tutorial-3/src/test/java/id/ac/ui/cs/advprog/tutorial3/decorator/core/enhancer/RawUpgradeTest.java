package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;
    private Weapon shield;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
        shield = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Shield", rawUpgrade.getName());
    }

    @Test
    public void testMethodGetDescription(){
        assertEquals(shield.getDescription() + " (+ raw)", rawUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(5 <= rawUpgrade.getWeaponValue()-shield.getWeaponValue() ||
        rawUpgrade.getWeaponValue()-shield.getWeaponValue() >= 10);
    }
}
