package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master", true);
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member ordMember = new OrdinaryMember("Dummy", "Clown");
        int oldSize = guild.getMemberList().size();
        guild.addMember(guildMaster, ordMember);
        assertEquals(oldSize+1, guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        Member ordMember = new OrdinaryMember("Dummy", "Clown");
        guild.addMember(guildMaster, ordMember);
        int oldSize = guild.getMemberList().size();
        guild.removeMember(guildMaster, ordMember);
        assertEquals(oldSize-1, guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member jono = guild.getMember("Jono", "Servant");
        assertEquals(null , jono);
        jono = new PremiumMember("Jono", "Servant");
        guild.addMember(guildMaster, jono);
        assertEquals(jono , guild.getMember("Jono", "Servant"));
    }
}
