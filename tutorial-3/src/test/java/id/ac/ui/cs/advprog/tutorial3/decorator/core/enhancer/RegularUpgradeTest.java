package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;
    private Weapon sword;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
        sword = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Sword", regularUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(sword.getDescription() + " (+ regular)", regularUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(1 <= regularUpgrade.getWeaponValue()-sword.getWeaponValue() ||
        regularUpgrade.getWeaponValue()-sword.getWeaponValue() >= 5);
    }
}
