package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    private Weapon gun;

    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
        gun = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Gun", chaosUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(gun.getDescription() + " (+ chaos)", chaosUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(50 <= chaosUpgrade.getWeaponValue()-gun.getWeaponValue() ||
        chaosUpgrade.getWeaponValue()-gun.getWeaponValue() >= 55);
    }

}
