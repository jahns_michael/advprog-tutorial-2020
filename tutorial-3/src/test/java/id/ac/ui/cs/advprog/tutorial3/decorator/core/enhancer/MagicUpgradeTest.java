package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;
    private Weapon longbow;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
        longbow = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Longbow", magicUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(longbow.getDescription() + " (+ magic)", magicUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(15 <= magicUpgrade.getWeaponValue()-longbow.getWeaponValue() ||
        magicUpgrade.getWeaponValue()-longbow.getWeaponValue() >= 20);
    }
}
