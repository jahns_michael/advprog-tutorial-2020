package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member dumMember = new OrdinaryMember();
        int oldSize = member.getChildMembers().size();
        member.addChildMember(dumMember);
        assertEquals(oldSize+1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member dumMember = new OrdinaryMember();
        member.addChildMember(dumMember);
        int oldSize = member.getChildMembers().size();
        member.removeChildMember(dumMember);
        assertEquals(oldSize-1, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member dummy1 = new OrdinaryMember();
        Member dummy2 = new OrdinaryMember();
        Member dummy3 = new OrdinaryMember();
        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        member = new PremiumMember("Wati", "Gold Merchant", true);
        Member dummy1 = new OrdinaryMember();
        Member dummy2 = new OrdinaryMember();
        Member dummy3 = new OrdinaryMember();
        Member dummy4 = new OrdinaryMember();
        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        member.addChildMember(dummy4);
        assertTrue(member.getChildMembers().size() > 3);
    }
}
