package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongbowTest {


    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Longbow", weapon.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals("Longbow adalah senjata fisik jarak jauh", weapon.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(15, weapon.getWeaponValue());
    }
}
