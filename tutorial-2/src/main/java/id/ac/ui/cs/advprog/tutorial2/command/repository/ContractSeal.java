package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;



@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private boolean undone = true;
    private Spell latestSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell spell = spells.get(spellName);
        spell.cast();
        latestSpell = spell;
        undone = false;
    }

    public void undoSpell() {
        if (undone) {
            if (latestSpell instanceof BlankSpell){
                latestSpell.undo();
                return;
            }
            else return;
        } 
        latestSpell.undo();
        undone = true;
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
