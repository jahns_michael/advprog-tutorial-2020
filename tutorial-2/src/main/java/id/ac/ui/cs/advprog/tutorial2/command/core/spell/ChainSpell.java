package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {

    protected ArrayList<Spell> spellList;

    public ChainSpell(ArrayList<Spell> spellList) {
        this.spellList = spellList;
    }
    
    public ChainSpell() {
        this(new ArrayList<Spell>());
    }

	@Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for (Spell spell : spellList) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spellList.size()-1; i >= 0; i--) {
            spellList.get(i).undo();
        }
    }
}
