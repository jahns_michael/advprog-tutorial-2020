package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() throws Exception {
        holyGrail = new HolyGrail();
    }

    @Test
    public void testMakeAWish() {
        holyGrail.makeAWish("Aku mau dapet nilai A.");
        assertEquals("Aku mau dapet nilai A.", HolyWish.getInstance().getWish());
    }

    @Test
    public void testgetHolyWish() {
        HolyWish holyWish = holyGrail.getHolyWish();
        assertEquals(HolyWish.getInstance(), holyWish);
    }
}
