package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.AgileAdventurer;
import id.ac.ui.cs.advprog.tutorial1.observer.core.KnightAdventurer;
import id.ac.ui.cs.advprog.tutorial1.observer.core.MysticAdventurer;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                //ToDo: Complete Me
                agileAdventurer = new AgileAdventurer(guild);
                knightAdventurer = new KnightAdventurer(guild);
                mysticAdventurer = new MysticAdventurer(guild);
                this.guild.add(agileAdventurer);
                this.guild.add(knightAdventurer);        
                this.guild.add(mysticAdventurer);           

        }

        //ToDo: Complete Me
        public void addQuest(Quest quest) {
                if (questRepository.save(quest) != null)
                guild.addQuest(quest);
        }

        public List<Adventurer> getAdventurers() {
                List<Adventurer> allAdventurers = new ArrayList<Adventurer>();
                allAdventurers.add(agileAdventurer);
                allAdventurers.add(knightAdventurer);
                allAdventurers.add(mysticAdventurer);
                return allAdventurers;
        };
}
