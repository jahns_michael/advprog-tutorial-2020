package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me

        public String attack() {
                return "attack with sword";
        }

        public String getType() {
                return "sword";
        }
}
